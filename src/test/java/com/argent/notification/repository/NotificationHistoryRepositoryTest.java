package com.argent.notification.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.argent.notification.entity.NotificationHistory;
import com.argent.notification.entity.NotificationType;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NotificationHistoryRepositoryTest {
    
    @Autowired
    private NotificationHistoryRepository notificationHistoryRepository;
    
    @Test
    public void testSaveNotificationHistory() {
        // given
        String walletAddress = "0x7672994c31d731c08cf876629d168efb90d06835";
        NotificationHistory notification = new NotificationHistory();
        notification.setWalletAddress(walletAddress);
        notification.setNotificationType(NotificationType.EMAIL);
        
        // when
        NotificationHistory savedNotification = notificationHistoryRepository.save(notification);
        
        // then
        assertNotNull(savedNotification);
        assertNotNull(savedNotification.getId());
        assertEquals(walletAddress, savedNotification.getWalletAddress());
        assertEquals(NotificationType.EMAIL, savedNotification.getNotificationType());
    }
    
}
