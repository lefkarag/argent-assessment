package com.argent.notification.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.argent.notification.entity.Notification;
import com.argent.notification.entity.NotificationType;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NotificationRepositoryTest {
    
    @Autowired
    private NotificationRepository notificationRepository;
    
    @Test
    public void testForExistingWallet() {
        // given
        String walletAddress = "0x7672994c31d731c08cf876629d168efb90d06835";
        
        // when
        List<Notification> notifications = notificationRepository.findEnabledNotificationsByWalletAddress(walletAddress);
        
        // then
        assertNotNull(notifications);
        assertEquals(1, notifications.size());
        assertEquals(walletAddress, notifications.get(0).getPk().getWalletAddress());
        assertEquals(NotificationType.EMAIL, notifications.get(0).getPk().getNotificationType());
    }
    
    @Test
    public void testForNonExistingWallet() {
        // given
        String walletAddress = "0x7672994c31d731c08cf876629d168efb90d06833";
        
        // when
        List<Notification> notifications = notificationRepository.findEnabledNotificationsByWalletAddress(walletAddress);
        
        // then
        assertNotNull(notifications);
        assertEquals(0, notifications.size());
    }
    
}
