package com.argent.notification.controller;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.argent.notification.NotificationServiceApplication;
import com.argent.notification.dto.NotificationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = NotificationServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class NotificationControllerTest {

    private String baseUrl;

    @Value("${local.server.port}")
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() throws MalformedURLException {
        baseUrl = new URL("http://localhost:" + port).toString();
    }

    @Test
    public void testPostNotifyExistingAddress() {
        // given
        NotificationRequest notification = new NotificationRequest();
        notification.setWalletAddress("0x7672994c31d731c08cf876629d168efb90d06835");
        notification.setTokenName("LefToken");
        notification.setTokenValue(10d);

        // when
        HttpEntity<?> entity = new HttpEntity<>(notification, buildHeaders());
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/notify", entity, Void.class);

        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testPostNotifyNonExistingAddress() {
        // given
        NotificationRequest notification = new NotificationRequest();
        notification.setWalletAddress("0x7672994c31d731c08cf876629d168efb90d06833");
        notification.setTokenName("LefToken");
        notification.setTokenValue(10d);

        // when
        HttpEntity<?> entity = new HttpEntity<>(notification, buildHeaders());
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/notify", entity, Void.class);

        // then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    
    @Test
    public void testPostNotifyInvalidAddress() {
        // given
        NotificationRequest notification = new NotificationRequest();
        notification.setWalletAddress("1234");
        notification.setTokenName("LefToken");
        notification.setTokenValue(10d);

        // when
        HttpEntity<?> entity = new HttpEntity<>(notification, buildHeaders());
        ResponseEntity<Void> response = restTemplate.postForEntity(baseUrl + "/notify", entity, Void.class);

        // then
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    private HttpHeaders buildHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", "Basic dXNlcjpwYXNz");
        return headers;
    }

}
