package com.argent.notification.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.argent.notification.dto.NotificationRequest;
import com.argent.notification.exception.AddressNotFoundException;
import com.argent.notification.service.NotificationService;

@RestController
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @PostMapping("/notify")
    public ResponseEntity<Void> notify(@Valid @RequestBody NotificationRequest notification) {
        try {
            notificationService.notify(notification.getWalletAddress(), notification.getTokenValue(), notification.getTokenName());
            return ResponseEntity.ok().build();
        } catch (AddressNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
