package com.argent.notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.argent.notification.entity.NotificationHistory;

public interface NotificationHistoryRepository extends JpaRepository<NotificationHistory, Integer> {
    
}
