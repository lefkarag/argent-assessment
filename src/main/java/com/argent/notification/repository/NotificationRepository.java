package com.argent.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.argent.notification.entity.Notification;
import com.argent.notification.entity.NotificationPK;

public interface NotificationRepository extends JpaRepository<Notification, NotificationPK> {

    @Query("select n from Notification n where n.pk.walletAddress = :walletAddress and n.enabled = 1")
    List<Notification> findEnabledNotificationsByWalletAddress(@Param("walletAddress") String walletAddress);
    
}
