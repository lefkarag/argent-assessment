package com.argent.notification.exception;

public class AddressNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public AddressNotFoundException(String msg) {
        super(msg);
    }

}
