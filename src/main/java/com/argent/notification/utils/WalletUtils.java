package com.argent.notification.utils;

public class WalletUtils {

    private static final String WALLET_ADDRESS_REGEX = "^0x[0-9a-f]{40}$";

    public static boolean isValidWalletAddress(String address) {
        return address.matches(WALLET_ADDRESS_REGEX);
    }

}
