package com.argent.notification.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.Data;

@Data
@Embeddable
public class NotificationPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private String walletAddress;

    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;

}
