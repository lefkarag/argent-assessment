package com.argent.notification.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 
 * Supported notification types
 * 
 * @author Lefteris Karageorgiou
 *
 */
@Getter
@AllArgsConstructor
public enum NotificationType {

    EMAIL("emailNotificationTypeService"), 
    SLACK("slackNotificationTypeService"), 
    SMS("smsNotificationTypeService");

    // Bean service name associated with the notification type implementation
    private String serviceName;

}
