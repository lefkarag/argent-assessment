package com.argent.notification.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.argent.notification.utils.WalletUtils;

public class WalletAddressValidator implements ConstraintValidator<WalletAddressConstraint, String> {

    @Override
    public boolean isValid(String address, ConstraintValidatorContext context) {
        return WalletUtils.isValidWalletAddress(address);
    }

}
