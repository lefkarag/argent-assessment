package com.argent.notification.service;

import com.argent.notification.exception.AddressNotFoundException;

public interface NotificationService {

    void notify(String walletAddress, Double tokenValue, String tokenName) throws AddressNotFoundException;
    
}
