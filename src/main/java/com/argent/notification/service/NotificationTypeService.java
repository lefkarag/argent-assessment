package com.argent.notification.service;

import java.util.Map;

public interface NotificationTypeService {
    
    static String TOKEN_VALUE = "tokenValue";
    static String TOKEN_NAME = "tokenName";

    void notify(String identifier, Map<String, Object> params);

}
