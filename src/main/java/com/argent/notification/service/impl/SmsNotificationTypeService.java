package com.argent.notification.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.argent.notification.service.NotificationTypeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("smsNotificationTypeService")
public class SmsNotificationTypeService implements NotificationTypeService {

    @Override
    public void notify(String identifier, Map<String, Object> params) {
        log.info("Sending sms to:{} with params:{}", identifier, params);
        // TODO implement SMS notification
    }
}
