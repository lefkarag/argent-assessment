package com.argent.notification.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.argent.notification.service.NotificationTypeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("emailNotificationTypeService")
public class EmailNotificationTypeService implements NotificationTypeService {

    @Override
    public void notify(String identifier, Map<String, Object> params) {
        log.info("Sending email to:{} with params:{}", identifier, params);
        // TODO implement Email notification
    }

}
