package com.argent.notification.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.argent.notification.entity.Notification;
import com.argent.notification.entity.NotificationHistory;
import com.argent.notification.entity.NotificationType;
import com.argent.notification.exception.AddressNotFoundException;
import com.argent.notification.repository.NotificationHistoryRepository;
import com.argent.notification.repository.NotificationRepository;
import com.argent.notification.service.NotificationService;
import com.argent.notification.service.NotificationTypeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {
    
    @Autowired
    private NotificationRepository notificationRepository;
    
    @Autowired
    private NotificationHistoryRepository notificationHistoryRepository;

    @Autowired
    private ApplicationContext context;
    
    @Override
    public void notify(String walletAddress, Double tokenValue, String tokenName) throws AddressNotFoundException {
        log.info("Finding enabled notifications for walletAddress:{}", walletAddress);
        List<Notification> notifications = notificationRepository.findEnabledNotificationsByWalletAddress(walletAddress);
        
        if (CollectionUtils.isEmpty(notifications)) {
            throw new AddressNotFoundException("Address " + walletAddress + " not found");
        }
        
        notifications.forEach(n -> {
            // send notification
            factory(n.getPk().getNotificationType()).notify(n.getNotificationTypeIdentifier(), buildParams(tokenValue, tokenName));
            
            // save notification history
            NotificationHistory notification = new NotificationHistory();
            notification.setWalletAddress(walletAddress);
            notification.setNotificationType(n.getPk().getNotificationType());
            notificationHistoryRepository.save(notification);
        });
    }
    
    // Builder pattern for the implementation of each notification type
    private NotificationTypeService factory(NotificationType type) {
        return context.getBean(type.getServiceName(), NotificationTypeService.class);
    }
    
    private Map<String, Object> buildParams(Double tokenValue, String tokenName) {
        Map<String, Object> map = new HashMap<>();
        map.put(NotificationTypeService.TOKEN_VALUE, tokenValue);
        map.put(NotificationTypeService.TOKEN_NAME, tokenName);
        return map;
    }

}
