package com.argent.notification.dto;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

import com.argent.notification.validator.WalletAddressConstraint;

import lombok.Data;

@Data
public class NotificationRequest {

    @WalletAddressConstraint
    private String walletAddress;

    @DecimalMin(value = "0", inclusive = false)
    private Double tokenValue;

    @NotBlank
    private String tokenName;
}
